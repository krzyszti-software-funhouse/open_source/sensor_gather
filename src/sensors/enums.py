from django.db import models


class Type(models.IntegerChoices):
    TEMPERATURE_AND_HUMIDITY = 0, "Temperature and Humidity"
