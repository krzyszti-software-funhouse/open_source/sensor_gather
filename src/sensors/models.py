from django.db import models

from sensors.enums import Type
from django.db.models import F


class Device(models.Model):
    name = models.CharField(max_length=256, unique=True)
    type = models.PositiveSmallIntegerField(choices=Type.choices, default=Type.TEMPERATURE_AND_HUMIDITY)


class ReadingManager(models.Manager):
    def get_queryset(self):
        queryset = super().get_queryset().order_by('-created_at')
        return queryset.annotate(temperature_with_two=F("temperature") + 2, humidity_with_twenty_two=F("humidity") + 22)


class Reading(models.Model):
    objects = ReadingManager()
    device = models.ForeignKey(Device, on_delete=models.CASCADE, related_name="readings")

    temperature = models.DecimalField(decimal_places=2, max_digits=4, null=True, blank=True)
    humidity = models.DecimalField(decimal_places=2, max_digits=4, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created_at', )
