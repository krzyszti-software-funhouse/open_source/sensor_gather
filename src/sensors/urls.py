from django.urls import path
from sensors import views
urlpatterns = [
    path('', views.SensorsData.as_view()),
    path('device/<int:pk>/', views.SensorsData.as_view(), name="device-detail"),
    path('api/reading/', views.CreateReadingAPIView.as_view()),
]
