from datetime import datetime, timedelta

from django.shortcuts import get_object_or_404
from django.views.generic import DetailView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView

from project.permissions import SecureAPIKeyAuthorized
from sensors.enums import Type
from sensors.models import Device, Reading


class SensorsData(DetailView):
    template_name = 'sensors.html'
    queryset = Device.objects.all().prefetch_related("readings")

    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        pk = self.kwargs.get("pk")
        if not pk:
            return queryset.first()
        return get_object_or_404(Device, pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["devices"] = Device.objects.all()
        return context


class CreateReadingAPIView(APIView):
    permission_classes = (SecureAPIKeyAuthorized,)

    def post(self, request, *args, **kwargs):
        data = request.data
        device_name = data["device_name"]
        temperature = data["temperature"]
        humidity = data["humidity"]

        Reading.objects.filter(created_at__lt=datetime.utcnow() - timedelta(days=7)).delete()

        device = Device.objects.get_or_create(name=device_name)[0]
        if device.type == Type.TEMPERATURE_AND_HUMIDITY:
            Reading.objects.create(device=device, temperature=temperature, humidity=humidity)
        return Response(data={}, status=HTTP_200_OK)
