from rest_framework.permissions import BasePermission
from project.settings import SECURITY_API_KEY


class SecureAPIKeyAuthorized(BasePermission):
    def has_permission(self, request, view, *args, **kwargs):
        return request.headers.get("Authorization", "") == f"X-Api-Key: {SECURITY_API_KEY}"
